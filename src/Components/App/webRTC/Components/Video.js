import React, { Component } from "react"
import { observer } from "mobx-react"
import VideoCall from "../Helpers/SimplePeer"
import io from 'socket.io-client'
import ShareScreenIcon from "./ShareScreenIcon"
import { getDisplayStream } from "../Helpers/MediaAccess"


@observer
class Video extends Component {
	videoCall = new VideoCall()

	constructor(props) {
		super(props)

		this.store = this.props.Store
	}

	componentDidMount() {
		const socket = io(process.env.REACT_APP_SIGNALING_SERVER)
		const component = this

		console.log(socket)
		this.store.setSocket(socket)
		const { roomId } = this.store

		this.getUserMedia().then(() => {
			socket.emit('join', {
				roomId: roomId
			})
		})

		socket.on('init', () => {
			component.store.setInitiator(true)
		})

		socket.on('ready', () => {
			component.enter(roomId)
		})

		socket.on('desc', data => {
			if (data.type === 'offer' && component.store.initiator) return
      if (data.type === 'answer' && !component.store.initiator) return
      component.call(data)
		})

		socket.on('disconnected', () => {
			component.store.setInitiator(true)
		})

		socket.on('full', () => {
      component.store.setFull(true)
    })
	}

	enter = roomId => {
		this.store.setConnecting(true)

		const peer = this.videoCall.init(
			this.store.localStream,
			this.store.initiator
		)
		console.log(peer)
		this.store.setPeer(peer)

		peer.on('signal', data => {
			const signal = {
				room: roomId,
				desc: data
			}

			console.log(this.store.socket)
			this.store.socket.emit('signal', signal)
		})

		peer.on('stream', stream => {
			this.remoteVideo.srcObject = stream
			this.store.setConnecting(false)
			this.store.setWaiting(false)
		})

		peer.on('error', (err) => {
			console.log(err)
		})
	}

	call = otherId => {
		this.videoCall.connect(otherId)
	}

	renderFull = () => {
		if(this.store.full) {
			return 'The room is full'
		}
	}

	getUserMedia(callback) {
		return new Promise((resolve, reject) => {
			console.log(navigator)
			navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia
			console.log(navigator.getUserMedia)

			const op = {
				video: {
					width: { min: 160, ideal: 640, max: 1280 },
          height: { min: 120, ideal: 360, max: 720 }
				},
				audio: true
			}
			console.log(op)
	
			navigator.getUserMedia(
				op,
				stream => {
					console.log(stream)
					this.store.setStreamUrl(stream)
					this.store.setLocalStream(stream)
					this.localVideo.srcObject = stream
					resolve()
				},
				() => {} 			
			)
		})
	}

	getDisplay() {
		getDisplayStream().then(stream => {
			console.log(stream)
			stream.oninactive = () => {
				this.store.peer.removeStream(this.store.localStream)
				this.getUserMedia().then(() => {
					this.store.peer.addStream(this.store.localStream)
				})
			}

			console.log(this.store.peer)
			console.log(stream.getTracks())
			this.store.setStreamUrl(stream)
			this.store.setLocalStream(stream)
			this.localVideo.srcObject = stream
			stream.getTracks().forEach(track => this.store.peer.addTrack(track, stream))
		})
	}

	render() {
		return (
      <div className="video-wrapper">
        <div className="local-video-wrapper">
          <video
            autoPlay
            id="localVideo"
            muted
            ref={video => (this.localVideo = video)}
          />
        </div>
        <video
          autoPlay
          className={`${
            this.store.connecting || this.store.waiting ? 'hide' : ''
          }`}
          id="remoteVideo"
          ref={video => (this.remoteVideo = video)}
        />
        <button className="share-screen-btn" onClick={() => {
          this.getDisplay()
        }}><ShareScreenIcon/></button>
        {this.store.connecting && (
          <div className="status">
            <p>Establishing connection...</p>
          </div>
        )}
        {this.store.waiting && (
          <div className="status">
            <p>Waiting for someone...</p>
          </div>
        )}
        {this.renderFull()}
      </div>
    )
	}
}

export default Video