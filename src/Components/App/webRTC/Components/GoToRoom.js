import React, { Component } from 'react'
import shortId from 'shortid'
import { observer } from 'mobx-react'
import Video from './Video'
import { observable } from 'mobx'


@observer
class GoToRoom extends Component {
	constructor(props) {
		super(props)

		this.store = props.Store			
	
		this.store.roomId = shortId.generate()
		this.showVideo = observable.box(false)
	}

	enterRoom(event) {
		event.preventDefault()
		this.showVideo.set(!this.showVideo.get())
	}

	render() {
		const { roomId } = this.store

		return (
			<div>
				<div className="enter-room-container">
					<form>
						<input type="text" value={roomId} placeholder="Room id" onChange={(event) => {
							this.store.setRoomId(event.target.value)
						}}/>
						<button onClick={(event) => {
							this.enterRoom(event)
						}}>Enter</button>
						</form>
				</div>
				
				<div>
					{
						this.showVideo.get() ?
						<Video Store = { this.store } />
						: null 
					}
				</div>
			</div>
		)
	}
}

export default GoToRoom