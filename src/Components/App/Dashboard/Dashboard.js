import React, { Component } from "react"
import Channels from "./Channels/Channels"
import { observable } from "mobx"
import { observer } from "mobx-react"
import ChannelForm from "./Channels/ChannelForm/ChannelForm"
import api from "../../../api/API"
import { Dropdown } from 'semantic-ui-react'
import Projects from "./Projects/Projects"


@observer
class Dashboard extends Component {
	constructor(props) {
		super(props)

		this.store = this.props.Store

		this.displayChannels = observable.box(false)
		this.displayChannelForm = observable.box(false)
		this.displayProjects = observable.box(false)

		api.on("authenticate", ({data}) => this.popUp(data) )
		this.getQueryParams()
	}

	getAuthUri() {
		api.send("authenticate") 
	}

	popUp(data) {
		let uri = data.uri

		window.open(uri, '_self')
	}

	toggleChannels = () => {
		this.displayChannels.set(!this.displayChannels.get())
	}

	toggleChannelForm = () => {
		this.displayChannelForm.set(!this.displayChannelForm.get())
	}

	toggleProjects = () => {
		this.displayProjects.set(!this.displayProjects.get())
	}

	getQueryParams(){
		let urlParams = new URLSearchParams(window.location.search)

		if(urlParams.has("code")){
			let code = urlParams.get("code")	
			
			api.send(
				"completeAuth", {
					code: code
				}
			)
		}
	}

	handleDropdownChange = (e, { value }) => {
		this.store.setOrganization(value)
	}
	
	render() {
		const { organizations } = this.store

		return (
			<div>
				<button onClick={ () => this.getAuthUri() }>
					Authenticate
				</button>

				<div>
					<Dropdown
						placeholder = "Select organization"
						fluid
						selection
						closeOnChange
						options = {
							organizations.map(org => {
								return {
									key: org.id,
									text: org.name,
									value: org.id
								}
							})
						}
						onChange = { this.handleDropdownChange }
					/>
				</div>

				{
					this.store.currentOrganization ?
					<div>
						<button onClick = { this.toggleChannels } >
							Channels
						</button>
					
						<button onClick = { this.toggleChannelForm }>
							Create Channel
						</button>
					</div>
					: null
				}

				<div>
					{ this.displayChannels.get() ? <Channels Store = { this.store } /> : null }
				</div>

				<div>
					{ this.displayChannelForm.get() ? <ChannelForm toggleChannelForm = { this.toggleChannelForm } Store = { this.store } /> : null }
				</div>
				
				<button onClick = { this.toggleProjects } >
					Projects
				</button>

				<div>
					{ this.displayProjects.get() ? <Projects Store = { this.store } /> : null }
				</div>
			</div>
		)
	}
}

export default Dashboard