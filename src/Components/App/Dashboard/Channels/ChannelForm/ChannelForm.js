import React, { Component } from "react"
import api from "../../../../../api/API"


class ChannelForm extends Component {
	constructor(props) {
		super(props)

		this.store = this.props.Store
		this.toggleChannelForm = this.props.toggleChannelForm
	}

	handleSubmit = (event) => {
		event.preventDefault()
		const data = new FormData(event.target)
	
		const name = data.get("name")
		const description = data.get("description")

		api.send("channel", {
			name,
			description,
			organization_id: this.store.currentOrganization.id
		})

		this.toggleChannelForm()
	}

	render() {
		return (
			<form onSubmit = { this.handleSubmit }>
				<div>
					<label>
						Channel Name:<br/>
						<input type = "text" name = "name" placeholder = "#general" />
					</label>
				</div>

				<div>
					<label>
						Channel Description:<br/>
						<input type = "textarea" name = "description" placeholder = "Write something about the channel" />
					</label>
				</div>

				<div>
					<input type = "submit" value = "Submit" />
				</div>
			</form>
		)
	}
}

export default ChannelForm