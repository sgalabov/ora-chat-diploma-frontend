import React, { Component } from "react"
import { observer } from "mobx-react"
import Chat from "../Chat/Chat"
import { Dropdown } from "semantic-ui-react"
import api from "../../../../api/API"

@observer
class Channels extends Component {
	constructor(props) {
		super(props)

		this.store = this.props.Store
	}

	changeChannel(channel) {
		this.store.setChannel(channel)
	}

	renderChannels(channels) {
		return (
			<div>
				{
					channels.map(channel => {
						return (
							<button key = { channel.id } onClick = { () => this.changeChannel(channel) } >
								{ channel.name }
							</button>
						)
					})
				}
			</div>
		)
	}

	removeMember(member){
		api.send("removeChannelMember", {
			channel_id: this.store.currentChannel.id,
			user_id: member.ora_id
		})
	}

	renderChannelMembers(channelMembers) {
		return (
			<div>
				{
					channelMembers.map(member => {
						return(
							<div key = { member.id } >
								{ member.full_name }
								{ this.store.currentUser.id !== member.ora_id ?
									<button onClick = { () => this.removeMember(member) } >
										Remove member
									</button>
									: null
								}
							</div>
						)
					})
				}
			</div>
		)
	}

	handleDropdownChange = (e, { value }) => {
		api.send("addChannelMember", {
			channel_id: this.store.currentChannel.id,
			user_id: value
		})
	}

	render() {
		const { channels, addableUsers, channelMembers } = this.store

		return (
			<div>
				{ this.renderChannels(channels) }

				<div>
					{ 
						this.store.currentChannel ?
						this.renderChannelMembers(channelMembers)
						: null
					}
				</div>

				<div>
					{ this.store.currentChannel ? 
						<Dropdown
							placeholder = "Add member"
							fluid
							selection
							closeOnChange
							options = {
								addableUsers.map(member => {
									return {
										key: member.id,
										text: member.full_name,
										value: member.id
									}
								})
							}
							onChange = { this.handleDropdownChange }
						/>
					: null}
				</div>

				<Chat Store = { this.store } />	
			</div>
		)
	}
}

export default Channels