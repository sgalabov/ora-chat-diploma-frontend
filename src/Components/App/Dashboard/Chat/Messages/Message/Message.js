import React, { Component } from "react"
import { observable, action } from "mobx"
import { observer } from "mobx-react"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faLaughBeam, faPen, faThumbsUp, faThumbsDown, faHeart } from '@fortawesome/free-solid-svg-icons'
import api from "../../../../../../api/API"


const icons = {
	"coffee": faCoffee,
	"laugh": faLaughBeam,
	"pen": faPen,
	"thumpsUp": faThumbsUp,
	"thumpsDown": faThumbsDown,
	"heart": faHeart
}


@observer
class Message extends Component {
	constructor(props) {
		super(props)

		this.store = this.props.Store
		this.message = this.props.message

		this.usedReactions = observable.box(this.message.reactions)
		this.usableReactions = observable.box(Object.keys(icons))
		this.reactionsTab = observable.box(false)
	}

	openReactionsTab = () => {
		this.reactionsTab.set(!this.reactionsTab.get())
	}

	addReaction = (type) => {
		api.on(
			"addReaction",
			action(res => {
				this.usedReactions.set(res.data.reactions)
			})
		)

		api.send(
			"addReaction",{
				name: type,
				message_id: this.message.id,
				channel_id: this.store.currentChannel.id
			}
		)

		this.usableReactions.get().splice(this.usableReactions.get().indexOf(type), 1)
	}

	removeReaction = (type, id) => {
		api.on(
			"removeReaction",
			action(res => {
				this.usedReactions.set(res.data.reactions)
			})
		)

		api.send(
			"removeReaction", {
				name: type,
				message_id: this.message.id,
				channel_id: this.store.currentChannel.id,
				reaction_id: id
			}
		)

		this.usableReactions.set([...this.usableReactions.get(), type])
	}

	render() {
		return(
			<div>
				<p>
					{this.message.created_by.full_name}: { this.message.content }
				</p>

				<div>
					{
						this.usedReactions.get().map(reaction => (
							<FontAwesomeIcon
							icon = { icons[reaction.name] }
							key = { reaction.id }
							onClick = { () => this.removeReaction(reaction.name, reaction.id) } />
						))
					}
				</div>

				<button onClick = { this.openReactionsTab } >
					Reaction
				</button>

				{
						this.reactionsTab.get() && 
						this.usableReactions.get().map(reactionType => (
							<FontAwesomeIcon 
							icon = { icons[reactionType]}
							key = { reactionType }
							onClick = { () => this.addReaction(reactionType) } />
						))
				}
			</div>
		)
	}
}


export default Message