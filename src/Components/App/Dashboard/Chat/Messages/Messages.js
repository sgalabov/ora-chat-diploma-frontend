import React, { Component } from "react"
import api from "../../../../../api/API"
import { observer } from "mobx-react"
import Message from "./Message/Message"

@observer
class Messages extends Component {
	constructor(props) {
		super(props)

		this.store = this.props.Store
	}
	
	createMessage = (event) => {
		if(event.key === "Enter"){
			api.send("message", {
				channel_id: this.store.currentChannel.id,
				content: this.store.messageContent
			})

			this.store.setContent("")
		}
	}

	handleChange = (event) => {
		this.store.setContent(event.target.value)
	}

	render() {
		return (
			<div>
				<div>
					{
						this.store.messages.map(message => {
							return (
								<div key = { message.id }>
									<Message Store = { this.store } message = { message } />
								</div>
							)
						})
					}
				</div>

				{
					this.store.currentChannel ?
					<input type = "text"
					onChange = { this.handleChange } 
					onKeyPress = { this.createMessage } 
					value = { this.store.messageContent } placeholder = "Enter the message here..." />
					: null
				}
				</div>
		)
	}
}

export default Messages