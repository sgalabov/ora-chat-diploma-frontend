import React, { Component } from "react"
import { observer } from "mobx-react";
import Messages from "./Messages/Messages";

@observer
class Chat extends Component {
	constructor(props) {
		super(props)

		this.store = this.props.Store
	}

	render() {

		// if (this.loading) {
		// 	return <Lottie />
		// }
		return (
			<div>
				<div>
					< Messages Store = { this.store } />
				</div>
			</div>
		)
	}
}

export default Chat