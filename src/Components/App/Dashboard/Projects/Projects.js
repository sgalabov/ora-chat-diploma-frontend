import React, { Component } from "react"
import { observer } from "mobx-react"
import Tasks from "./Tasks/Tasks"


@observer
class Projects extends Component {
	constructor(props) {
		super(props)

		this.store = this.props.Store

	}

	changeProject = (projectId) => {
		this.store.setProject(projectId)
	}

	renderProjects = (projects) => {
		return (
			<div>
				{
					projects.map(project => {
						return (
							<button key = { project.id } onClick = { () => this.changeProject(project) } >
								{ project.title }
							</button>
						)
					})
				}
			</div>
		)
	}

	render() {
		const { projects } = this.store

		return (
			<div>
				{ this.renderProjects(projects) }	

				<Tasks Store = { this.store } />
			</div>
		)
	}
}


export default Projects