import React, { Component } from "react"
import { observer } from "mobx-react"


@observer
class Task extends Component{
	constructor(props) {
		super(props)
		this.store = this.props.Store
	}

	renderComments(comments) {
		return (
			comments.map(comment => {
				return (
					<p key = { comment.id }>
						{ comment.comment }
					</p>
				)
			})
		)
	}

	render() {
		const task = this.store.currentTask
		const { taskComments } = this.store

		return (
			<div>
				<p>
					{ task.title }
					{ task.creator }
				</p>

				<div>
					{ this.renderComments(taskComments) }
				</div>

			</div>
		)
	}
}

export default Task