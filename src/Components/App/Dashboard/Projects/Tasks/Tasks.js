import React, { Component } from "react"
import { observer } from "mobx-react"
import Task from "./Task/Task"


@observer
class Tasks extends Component{
	constructor(props) {
		super(props)

		this.store = this.props.Store
	}

	changeTask(task) {
		this.store.setTask(task)
	}

	renderTasks(tasks) {
		return (
			<div>
				{
					tasks.map(task => {
						return (
							<button key = { task.id } onClick = { () => this.changeTask(task) } >
								{ task.title }
							</button>
						)
					})
				}
			</div>
		)
	}

	render(){
		const { tasks } = this.store

		return (
			<div>
					{ this.renderTasks(tasks) }

					{ this.store.currentTask ?
						<div>
							<Task Store = { this.store } />
						</div>
					: null }
			</div>
		)
	}

}

export default Tasks