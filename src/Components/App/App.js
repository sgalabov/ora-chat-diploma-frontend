import React from 'react';
import Dashboard from './Dashboard/Dashboard'
import ChatStore from "../../Store/ChatStore"
import 'bootstrap/dist/css/bootstrap.min.css'
import 'semantic-ui-css/semantic.min.css'
import GoToRoom from './webRTC/Components/GoToRoom';
import WebRTCStore from '../../Store/WebRTCStore';


const App = () => (
	<div>
		<Dashboard Store = { ChatStore }/>
		<GoToRoom Store = { WebRTCStore }/>
	</div>
)

export default App
