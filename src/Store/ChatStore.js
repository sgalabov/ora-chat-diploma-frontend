import { observable, action } from "mobx"
import api from "../api/API"


class ChatStore {
	@observable currentUser = {} 
	@observable channels = []
	@observable organizations = []
	@observable organizationMembers = []
	@observable messages = []
	@observable currentChannel
	@observable channelMembers = []
	@observable directChannels = []
	@observable addableUsers = []
	@observable currentOrganization
	@observable currentProject
	@observable currentTask
	@observable messageContent = ""
	@observable projects = []
	@observable tasks = []
	@observable taskList
	@observable taskComments = []

	constructor() {
		api.on(
			"completeAuth",
			action(res => {
				this.currentUser = res.data.user
				localStorage.setItem("token", res.data.token)

				api.send("organizations")

				window.history.pushState("", "", "/")
			})
		)

		api.on(
			"organizations",
			action(res => {			
				this.organizations = res.data.organizations
			})
		)

		api.on(
			"organization",
			action(res => {
				this.currentOrganization = res.data.organization
			
				api.send("channels", {
					organization_id: this.currentOrganization.id
				})
			})
		)

		api.on(
			"organizationMembers",
			action(res => {
				this.organizationMembers = res.data.members.developers
			})
		)

		api.on(
			"channel",
			action(res => {
				this.channels.push(res.data)
			})
		)

		api.on(
			"channelMembers",
			action(res => {
				this.channelMembers = res.data.members

				this.changeAddableUsers()
			})
		)

		api.on(
			"addChannelMember",
			action(res => {
				this.channelMembers = res.data.members
			
				this.changeAddableUsers()
			})
		)

		api.on(
			"removeChannelMember",
			action(res => {
				this.channelMembers = res.data.members

				this.changeAddableUsers()
			})
		)

		api.on(
			"channels",
			action(res => {
				this.channels = res.data.channels
				this.directChannels = res.data.direct_channels
			})
		)

		api.on(
			"messages",
			action(res => {
				this.messages = res.data.messages.results
			})
		)

		api.on(
			"message",
			action(res => {
				this.messages.push(res.data)
			})
		)

		api.on(
			"projects",
			action(res => {
				this.projects = res.data.projects
			})
		)

		api.on(
			"tasks",
			action(res => {
				this.tasks = res.data.tasks
			})
		)

		api.on(
			"taskList",
			action(res => {
				this.taskList = res.data.list
			})
		)

		api.on(
			"taskComments",
			action(res => {
				this.taskComments = res.data.comments
			})
		)
	}

	changeAddableUsers(){
		this.addableUsers = this.organizationMembers.filter((member) => {
			const inChannelAlready = this.channelMembers.filter((el) => {
				return el.ora_id === member.id
			})

			if (Object.keys(inChannelAlready).length !== 0){
				return false
			} 

			return true
		})
	}

	@action setContent(value){
		this.messageContent = value
	}

	@action setOrganization(organization_id){
		api.send("organization", {
			organization_id
		})

		api.send("organizationMembers", {
			organization_id
		})

		api.send("projects", {
			organization_id
		})
	}

	@action setProject(project){
		this.currentProject = project

		api.send("tasks", {
			project_id: this.currentProject.id,
		})
	}

	@action setChannel(channel){
		this.currentChannel = channel

		api.send("channelMembers", {
			channel_id: this.currentChannel.id
		})

		api.send("messages", {
			channel_id: this.currentChannel.id
		})
	}

	@action setTask(task){
		this.currentTask = task

		api.send("taskList", {
			project_id: this.currentProject.id,
			task_id: this.currentTask.id,
			list_id: this.currentTask.list_id
		})

		api.send("taskComments", {
			project_id: this.currentProject.id,
			task_id: this.currentTask.id
		})
	}
}

export default new ChatStore()