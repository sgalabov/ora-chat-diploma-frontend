import { observable, action } from "mobx"


class WebRTCStore {
	@observable roomId
	@observable localStream = {}
	@observable remoteStreamUrl = ""
	@observable streamUrl = ""
	@observable initiator = false
	@observable peer = {}
	@observable full = false
	@observable connecting = false
	@observable waiting = true
	@observable socket

	constructor() {
		console.log("")
	}

	@action setRoomId(roomId){
		this.roomId = roomId
	}

	@action setSocket(socket){
		this.socket = socket
	}

	@action setStreamUrl(stream){
		this.streamUrl = stream
	}

	@action setLocalStream(stream){
		this.localStream = stream
	}

	@action setInitiator(value){
		this.initiator = value
	}

	@action setFull(value){
		this.full = value
	}

	@action setWaiting(value){
		this.waiting = value
	}

	@action setConnecting(value){
		this.connecting = value
	}

	@action setPeer(peer){
		this.peer = peer
	}
}

export default new WebRTCStore()